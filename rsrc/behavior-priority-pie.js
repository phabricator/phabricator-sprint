/**
 * @provides javelin-behavior-priority-pie
 */

JX.behavior('priority-pie', function(config) {

    var priorityPoints = new Array();
    var data = config.Priorities.split(";");

    var len = data.length -1;
    for (x = 0; x < len; x = x + 2) {
      var priority = new Array();
      priority[0] = data[x];
      priority[1] = data[x+1];
      priorityPoints.push(priority);
    }

    var h = JX.$(config.hardpoint);
    var l = c3.generate({
        bindto: h,
        data: {
            columns: priorityPoints,
            type: 'pie'
        },
    });
});
