/**
 * @provides javelin-behavior-c3-board-data-pie
 */

JX.behavior('c3-board-data-pie', function(config) {

    var columnPoints = new Array();
    var data = config.Columns.split(";");

    var len = data.length -1;
    for (x = 0; x < len; x = x + 2) {
      var column = new Array();
      column[0] = data[x];
      column[1] = data[x+1];
      columnPoints.push(column);
    }

    var h = JX.$(config.hardpoint);
    var l = c3.generate({
        bindto: h,
        data: {
            columns: columnPoints,
            type: 'pie'
        },
    });
});
