<?php

final class BoardDataPieView extends Phobject {

  private $boardData;

  public function setBoardData($board_data) {
    $this->boardData = $board_data;
    return $this;
  }

  public function buildPieBox() {
    $this->initBoardDataPie();
    $this->initTaskStatusPie();
    $this->initTaskPriorityPie();
    $project_name = $this->boardData->getProject()->getName();
    $boardpie = phutil_tag('div',
        array(
            'id' => 'c3-board-data-pie',
            'style' => 'width: 265px; height:200px; padding-left: 30px;
                float: left;',
        ), pht('Board'));
    $taskpie = phutil_tag('div',
        array(
            'id' => 'pie',
            'style' => 'width: 225px; height:200px; padding-left: 170px;
            float: left;',
        ), pht('Task Status'));
    $priority_pie = phutil_tag('div',
        array(
            'id' => 'priority-pie',
            'style' => 'width: 500px; height:200px; padding-left: 75px;
            margin-left: 750px;',
        ), pht('Task Priority'));

    $box = id(new SprintUIObjectBoxView())
        ->setHeaderText(pht('Points Allocation Report for '.
            $project_name))
        ->setColor('green')
        ->appendChild($boardpie)
        ->appendChild($taskpie)
        ->appendChild($priority_pie);

    return $box;
  }

  private function initBoardDataPie() {
    require_celerity_resource('d3', 'sprint');
    require_celerity_resource('c3-css', 'sprint');
    require_celerity_resource('c3', 'sprint');

    $coldata = $this->boardData->getColumnData();
    $columns = "";

    foreach ($coldata as $col) {
      $col_name = $col[0];
      $col_points = $col[2];
      $columns .= $col_name . ";" . $col_points . ";";
    }

    $id = 'c3-board-data-pie';
    Javelin::initBehavior('c3-board-data-pie', array(
        'hardpoint' => $id,
        'Columns' => $columns,
    ), 'sprint');

  }

  private function initTaskStatusPie() {
    $sprintpoints = id(new SprintPoints())
        ->setTasks($this->boardData->getTasks());

    list($task_open_status_sum, $task_closed_status_sum) = $sprintpoints
        ->getStatusSums();

    require_celerity_resource('d3', 'sprint');
    require_celerity_resource('c3-css', 'sprint');
    require_celerity_resource('c3', 'sprint');

    $id = 'pie';
    Javelin::initBehavior('c3-pie', array(
        'hardpoint' => $id,
        'open' => $task_open_status_sum,
        'resolved' => $task_closed_status_sum,
    ), 'sprint');
  }

  private function initTaskPriorityPie() {
    $sprintpoints = id(new SprintPoints())
        ->setTasks($this->boardData->getTasks());

    $task_priority_sum = $sprintpoints
        ->getPrioritySums();

    $priorities = "";

    foreach(array_keys($task_priority_sum) as $key) {
      $priorities .= $key . ";" . $task_priority_sum[$key] . ";";
    }

    require_celerity_resource('d3', 'sprint');
    require_celerity_resource('c3-css', 'sprint');
    require_celerity_resource('c3', 'sprint');

    $id = 'priority-pie';
    Javelin::initBehavior('priority-pie', array(
        'hardpoint' => $id,
        'Priorities' => $priorities,
    ), 'sprint');
  }
}
