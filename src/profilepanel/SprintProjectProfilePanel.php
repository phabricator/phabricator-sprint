<?php

final class SprintProjectProfilePanel
    extends PhabricatorProfileMenuItem {

  const MENUITEMKEY = 'project.sprint';

  public function getMenuItemTypeName() {
    return pht('Project Burndown');
  }

  private function getDefaultName() {
    return pht('Burndown');
  }

  public function shouldEnableForObject($object) {
    $sprint = id(new SprintQuery())
      ->setViewer($this->getViewer())
      ->setPHID($object->getPHID());

    if (!$sprint) {
      return null;
    }

    $is_sprint = $sprint->getIsSprint();
    if (!$is_sprint) {
      return false;
    }

    return true;
  }

  public function getDisplayName(
      PhabricatorProfileMenuItemConfiguration $config) {
    $name = $config->getMenuItemProperty('name');

    if (strlen($name)) {
      return $name;
    }

    return $this->getDefaultName();
  }

  public function buildEditEngineFields(
      PhabricatorProfileMenuItemConfiguration $config) {
    return array(
        id(new PhabricatorTextEditField())
            ->setKey('name')
            ->setLabel(pht('Name'))
            ->setPlaceholder($this->getDefaultName())
            ->setValue($config->getPanelProperty('name')),
    );
  }

  protected function newNavigationMenuItems(
      PhabricatorProfileMenuItemConfiguration $config) {

    $project = $config->getProfileObject();
    $id = $project->getID();

    $name = $this->getDisplayName($config);
    $icon = 'fa-fire';
    $href = "/project/sprint/view/{$id}/";

    $item = $this->newItem()
        ->setHref($href)
        ->setName($name)
        ->setIcon($icon);

    return array(
        $item,
    );
  }

}
