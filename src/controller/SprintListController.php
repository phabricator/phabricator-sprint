<?php

final class SprintListController extends SprintController {
  public function handleRequest(AphrontRequest $request) {
    $uri = $request->getRequestURI();
    $uri->setPath('/project/');

    return id(new AphrontRedirectResponse())->setURI($uri);
  }
}
