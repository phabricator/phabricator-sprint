<?php

final class SprintProjectBoardController
    extends SprintController {

  public function shouldAllowPublic() {
    return true;
  }

  public function handleRequest(AphrontRequest $request) {
    $uri = $request->getRequestURI();
    $projectID = $request->getURIData('id');
    $uri->setPath("/project/board/{$projectID}/");
    return id(new AphrontRedirectResponse())->setURI($uri);
  }

}
