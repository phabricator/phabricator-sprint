<?php

/**
 * @author Michael Peters
 * @author Christopher Johnson
 * @license GPL version 3
 */

abstract class SprintController extends PhabricatorController {

  public function shouldAllowPublic() {
    return true;
   }

  public function getUser() {
    return $this->getRequest()->getUser();
  }

  public function setApplicationURI() {
    return new PhutilURI($this->getApplicationURI());
  }

  public function getProjectIDfromSlug($slug, $viewer) {
    $project = id(new PhabricatorProjectQuery())
        ->setViewer($viewer)
        ->withSlugs(array($slug))
        ->executeOne();
    $id = $project->getID();
    return $id;
  }

  protected function isSprint($object) {
    $validator = new SprintValidator();
    $issprint = call_user_func(array($validator, 'checkForSprint'),
        array($validator, 'isSprint'), $object->getPHID());
    return $issprint;
  }

  public function getErrorBox($e) {
    $error_box = id(new PHUIInfoView())
        ->setTitle(pht('Sprint could not be rendered for this project'))
        ->setErrors(array($e->getMessage()));
    return $error_box;
  }
}
