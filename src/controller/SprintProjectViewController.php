<?php

final class SprintProjectViewController
    extends SprintController {

  public function shouldAllowPublic() {
    return true;
  }

  public function handleRequest(AphrontRequest $request) {
    $uri = $request->getRequestURI();
    $projectID = $request->getURIData('id');
    $uri->setPath("/project/view/{$projectID}/");
    return id(new AphrontRedirectResponse())->setURI($uri);
  }

}
