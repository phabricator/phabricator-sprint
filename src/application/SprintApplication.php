<?php

/**
 * @author Michael Peters
 * @author Christopher Johnson
 * @license GPL version 3
 */

final class SprintApplication extends PhabricatorApplication {

  public function getName() {
    return pht('Sprint');
  }

  public function getBaseURI() {
      return '/project/sprint/';
  }

  public function getIconName() {
    return 'fa-puzzle-piece';
  }

  public function getShortDescription() {
    return 'Build Sprints';
  }

  public function getRoutes() {
      return array(
          '/project/sprint/' => array(
	      // redirects to core projects app
              '' => 'SprintListController',
              'profile/(?P<id>[1-9]\d*)/'
              => 'SprintProjectProfileController',
              'board/(?P<id>\d+)/' => 'SprintProjectBoardController',
              // still contained within Sprint all
              'burn/(?P<id>\d+)/' => 'SprintDataViewController',
              'view/(?P<id>\d+)/' => 'SprintDataViewController',
              'report/list/' => 'SprintListController',
              'report/history/' => 'SprintHistoryController',
              'report/(?:(?P<view>\w+)/)?' => 'SprintReportController',
          ),
      );
    }
}
