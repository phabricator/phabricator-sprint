<?php

$root = dirname(dirname(__FILE__));
require_once $root.'/constants/SprintConstants.php';
require_once $root.'/tests/Autoloader.php';
require_once $root.'/../../libphutil/src/internationalization/pht.php';
require_once $root.'/../../libphutil/src/utils/utils.php';
require_once $root.'/../../libphutil/src/moduleutils/core.php';
require_once $root.'/../../libphutil/src/moduleutils/moduleutils.php';
require_once $root.'/../../libphutil/src/markup/render.php';
AutoLoader::registerDirectory($root);
AutoLoader::registerDirectory($root.'/../../phabricator');
AutoLoader::registerDirectory($root.'/../../libphutil');
