<?php

final class SprintColumnTransaction {

  private $viewer;
  private $project;
  private $query;
  private $tasks;
  private $events;
  private $xquery;

  public function setViewer($viewer) {
    $this->viewer = $viewer;
    return $this;
  }

  public function setProject($project) {
    $this->project = $project;
    return $this;
  }

  public function setQuery($query) {
    $this->query = $query;
    return $this;
  }

  public function setTasks($tasks) {
    $this->tasks = $tasks;
    return $this;
  }

  public function getXactionsforColumn(
      PhabricatorApplicationTransactionInterface $column) {
    $xactions = $this->xquery
        ->setViewer($this->viewer)
        ->withObjectPHIDs(array($column->getPHID()))
        ->needComments(true)
        ->setReversePaging(false)
        ->execute();
    $xactions = array_reverse($xactions);
    return $xactions;
  }

  public function parseEvents($dates) {

    $sprintpoints = id(new SprintPoints())
        ->setTasks($this->tasks)
        ->setViewer($this->viewer);

    foreach ($this->events as $event) {
      $modify_date = $event['modified'];
      $task_phid = $event['objectPHID'];

      $points = $sprintpoints->getTaskPointsbyPHID($task_phid);

      $date = phabricator_format_local_time($modify_date,
          $this->viewer, 'D M j');

       switch ($event['type']) {
          case 'close':
            $this->closeTasksToday($date, $dates);
            $this->closePointsToday($date, $points, $dates);
            break;
          case 'open':
            $this->openTasksToday($date, $dates);
            $this->openPointsToday($date, $points, $dates);
            break;
          case 'reopen':
            $this->reopenedTasksToday($date, $dates);
            $this->reopenedPointsToday($date, $points, $dates);
            break;
        }
      }
    return $dates;
  }

  private function closeTasksToday($date, $dates) {
    $dates[$date]->setTasksClosedToday();
    return $dates;
  }

  private function closePointsToday($date, $points, $dates) {
    $dates[$date]->setPointsClosedToday($points);
    return $dates;
  }

  private function openTasksToday($date, $dates) {
    $dates[$date]->setTasksAddedToday();
    return $dates;
  }

  private function openPointsToday($date, $points, $dates) {
    $dates[$date]->setPointsAddedToday($points);
    return $dates;
  }

  private function reopenedPointsToday($date, $points, $dates) {
    $dates[$date]->setPointsReopenedToday($points);
    return $dates;
  }

  private function reopenedTasksToday($date, $dates) {
    $dates[$date]->setTasksReopenedToday();
    return $dates;
  }

 private function setXActionTaskStatusEventType($was_open, $is_open) {
    if ($was_open == $is_open) {
      return null; // no change, ignore
    }
    if ($was_open && !$is_open) {
      return 'close'; // task has been closed
    } else {
      return 'reopen'; // task was closed and has been reopened
    }
  }

  public function setEventsTaskStatus($xactions) {
    //assert_instances_of($xactions, 'ManiphestTransaction');
    $events = array();
    foreach ($xactions as $xaction) {
      if ($xaction->getOldValue() == null) {
        if (ManiphestTaskStatus::isOpenStatus($xaction->getNewValue())) {
            $event_type = 'open';
        } else {
            $event_type = 'close';
        }
      } else {
        $was_open = ManiphestTaskStatus::isOpenStatus($xaction->getOldValue());
        $is_open = ManiphestTaskStatus::isOpenStatus($xaction->getNewValue());
        $event_type = $this->setXActionTaskStatusEventType($was_open, $is_open);
      }
      if ($event_type !== null) {
        $events[] = array(
            'transactionPHID' => $xaction->getPHID(),
            'objectPHID' => $xaction->getObjectPHID(),
            'created' => $xaction->getDateCreated(),
            'modified' => $xaction->getDateModified(),
            'key'   => $xaction->getMetadataValue('customfield:key'),
            'type'  => $event_type,
            'title' => $xaction->getTitle(),
        );
      }
    }

    $this->events = isort($events, 'modified');

    return $this;
  }
}
