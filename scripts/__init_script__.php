<?php

function init_phabricator_script() {

  phutil_load_library(dirname(__FILE__).'/../../libphutil/src');
  phutil_load_library(dirname(__FILE__).'/../../phabricator/src');
  phutil_load_library(dirname(__FILE__).'/../src');
  phutil_load_library(dirname(__FILE__).'/../../arcanist/src');


  PhabricatorEnv::initializeScriptEnvironment();
}

init_phabricator_script();
